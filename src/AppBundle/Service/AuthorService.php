<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

class AuthorService
{
    /**
     * @var EntityRepository
     */
    private $repository;

    public function __construct(EntityManager $manager)
    {
        $this->repository = $manager->getRepository('AppBundle:Author');
    }

    public function findById($id)
    {
        return $this->repository->find($id);
    }

    public function findAll()
    {
        return $this->repository->findAll();
    }

    public function asArray($authors)
    {
        // probably there is a way of doing this automatically with symfony
        $response = [];
        if (is_array($authors))
        {
            foreach ($authors as $author)
            {
                $response[] = [
                    'id'          => $author->getId(),
                    'name'        => $author->getName(),
                    'birthday'    => $author->getBirthday(),
                    'nationality' => $author->getNationality(),
                ];
            }
            return $response;
        }

        $response['id']          = $authors->getId();
        $response['name']        = $authors->getName();
        $response['birthday']    = $authors->getBirthday();
        $response['nationality'] = $authors->getNationality();

        return $response;
    }
}