<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

class BookService
{
    /**
     * @var EntityRepository
     */
    private $repository;

    public function __construct(EntityManager $manager)
    {
        $this->repository = $manager->getRepository('AppBundle:Book');
    }

    public function findById($id)
    {
        return $this->repository->find($id);
    }

    public function findAll()
    {
        return $this->repository->findAll();
    }

    public function findAllHighlighted()
    {
        return $this->repository->findAllHighlighted();
    }

    public function searchByTitle($titleQuery)
    {
        return $this->repository->findAllByTitle($titleQuery);
    }

    public function searchByTitlePaginated($titleQuery, $offset, $limit)
    {
        return $this->repository->findAllByTitlePaginated($titleQuery, $offset, $limit);
    }

    public function asArray($books)
    {
        // probably there is a way of doing this automatically with symfony
        $response = [];
        if (is_array($books))
        {
            foreach ($books as $book)
            {
                $response[] = [
                    'id'           => $book->getId(),
                    'title'        => $book->getTitle(),
                    'author'       => $book->getAuthor()->getName(),
                    'publisher'    => $book->getPublisher()->getName(),
                    'release_date' => $book->getReleaseDate()
                ];
            }
            return $response;
        }

        $response['id']           = $books->getId();
        $response['title']        = $books->getTitle();
        $response['author']       = $books->getAuthor()->getName();
        $response['publisher']    = $books->getPublisher()->getName();
        $response['release_date'] = $books->getReleaseDate();

        return $response;
    }
}