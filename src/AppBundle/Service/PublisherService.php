<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

class PublisherService
{
    /**
     * @var EntityRepository
     */
    private $repository;

    public function __construct(EntityManager $manager)
    {
        $this->repository = $manager->getRepository('AppBundle:Publisher');
    }

    public function findById($id)
    {
        return $this->repository->find($id);
    }

    public function findAll()
    {
        return $this->repository->findAll();
    }

    public function asArray($publishers)
    {
        // probably there is a way of doing this automatically with symfony
        $response = [];
        if (is_array($publishers))
        {
            foreach ($publishers as $publisher)
            {
                $response[] = [
                    'id'         => $publisher->getId(),
                    'name'       => $publisher->getName(),
                    'founded_at' => $publisher->getFoundedAt()
                ];
            }
            return $response;
        }

        $response['id']         = $publishers->getId();
        $response['name']       = $publishers->getName();
        $response['founded_at'] = $publishers->getFoundedAt();

        return $response;
    }
}