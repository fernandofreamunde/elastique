<?php

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class BookController extends Controller
{
    /**
     * @Route("/books")
     */
    public function indexAction()
    {
        $bookService = $this->get('app.service.book');
        $books = $bookService->findAll();

        return new JsonResponse($bookService->asArray($books));
    }

    /**
     * @Route("/books/highlighted")
     */
    public function highlightedAction()
    {
        $bookService = $this->get('app.service.book');
        $books       = $bookService->findAllHighlighted();

        return new JsonResponse($bookService->asArray($books));
    }

    /**
     * @Route("/books/{id}")
     */
    public function showAction(Book $id)
    {
        $bookService = $this->get('app.service.book');
        $book        = $bookService->findById($id);

        if (!$book)
        {
            return new JsonResponse(['404' => 'not found']);
        }

        return new JsonResponse($bookService->asArray($book));
    }

    /**
     * @Route("/books/search/{titleQuery}/{offset}/{limit}")
     */
    public function searchAction($titleQuery, $offset = null, $limit = null)
    {
        $bookService = $this->get('app.service.book');

        if (!is_null($offset) && !is_null($limit))
        {
            $book = $bookService->searchByTitlePaginated($titleQuery, $offset, $limit);
            return new JsonResponse($bookService->asArray($book));
        }

        $book = $bookService->searchByTitle($titleQuery);

        return new JsonResponse($bookService->asArray($book));
    }
}