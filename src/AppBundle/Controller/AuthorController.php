<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class AuthorController extends Controller
{
    /**
     * @Route("/authors")
     * @Route("/authors/list")
     */
    public function indexAction()
    {
        $authorService = $this->get('app.service.author');
        $authors       = $authorService->findAll();

        return new JsonResponse($authorService->asArray($authors));
    }

    /**
     * @Route("/authors/{id}")
     */
    public function showAction($id)
    {
        $authorService = $this->get('app.service.author');
        $author        = $authorService->findById($id);

        if (!$author)
        {
            return new JsonResponse(['404' => 'not found']);
        }

        return new JsonResponse($authorService->asArray($author));
    }
}