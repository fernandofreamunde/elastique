<?php

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class PublisherController extends Controller
{
    /**
     * @Route("/publishers")
     * @Route("/publishers/list")
     */
    public function indexAction()
    {
        $publisherService = $this->get('app.service.publisher');
        $publishers       = $publisherService->findAll();

        return new JsonResponse($publisherService->asArray($publishers));
    }

    /**
     * @Route("/publishers/{id}")
     */
    public function showAction($id)
    {
        $publisherService = $this->get('app.service.publisher');
        $publisher        = $publisherService->findById($id);

        if (!$publisher)
        {
            return new JsonResponse(['404' => 'not found']);
        }

        return new JsonResponse($publisherService->asArray($publisher));
    }
}