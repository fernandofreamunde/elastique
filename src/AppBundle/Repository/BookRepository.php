<?php
/**
 * Created by PhpStorm.
 * User: fernando
 * Date: 7/28/17
 * Time: 12:52 AM
 */

namespace AppBundle\Repository;
use Doctrine\ORM\EntityRepository;

class BookRepository extends EntityRepository
{
    public function findAllByTitle($title)
    {
        return $this->createQueryBuilder('book')
            ->andWhere('book.title Like :title')
            ->setParameter('title', '%'.$title.'%')
            ->getQuery()
            ->execute();
    }

    public function findAllHighlighted()
    {
        return $this->createQueryBuilder('book')
            ->andWhere('book.isHighlighted Like :highlighted')
            ->setParameter('highlighted', true)
            ->getQuery()
            ->execute();
    }

    public function findAllByTitlePaginated($title, $offset, $limit)
    {
        return $this->createQueryBuilder('book')
            ->andWhere('book.title Like :title')
            ->setParameter('title', '%'.$title.'%')
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getQuery()
            ->execute();
    }

}