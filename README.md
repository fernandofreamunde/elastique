elastique
=========

A Symfony project created on July 26, 2017, 6:35 pm.
This project was created for an assessment for a company called Elastique and the requirements are a s follows:


## Introduction
A client (Elastique) would like to have a RESTful API where a user can easily search
through a database of books.

### The assignment
**Build a RESTful API that:**
- displays a list of featured books
- and allows the user to search through all books

You are free to use libraries or write code from scratch. Send the solution with the source code (zip file) and the url where the REST API is available.

The following calls needs to be available:

#### Publishers
- All publishers
    - http://assessment.elastique.nl/publishers/list
- A specific publisher by ID:
    - http://assessment.elastique.nl/publishers/14

#### Authors
- All authors
    - http://assessment.elastique.nl/authors/list
- A specific author by ID:
    - http://assessment.elastique.nl/authors/5

#### Books
- All featured/highlighted items:
    - http://assessment.elastique.nl/books/highlighted
- A specific book by ID:
    - http://assessment.elastique.nl/books/49
- Search by keyword ( optional offset / limit ):
    - http://assessment.elastique.nl/books/search/elastique
    - http://assessment.elastique.nl/books/search/elastique/0/1

#### Bonus points for...
- ~~Unit/integration tests~~
- ~~Caching~~

#### Notes
Due to unforeseeable events in my personal life that shortened my time available for this assessment, I could not aim to deliver the bonus points.